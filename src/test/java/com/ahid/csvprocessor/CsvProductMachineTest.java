package com.ahid.csvprocessor;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CsvProductMachineTest {

    private static CsvProductMachine csvProductMachine;

    @BeforeAll
    static void setup() {
        csvProductMachine = new CsvProductMachine(CsvProductMachineTest.class.getResource("/sample-csvs").getPath(), 10, 2, 4);
    }

    @Test
    public void preparedFilesTest() {
        assertEquals(csvProductMachine.preparedFiles().size(), 5);
    }

    @Test
    public void getResultsWhenProcessingWasNotFinishedTest() {
        assertThrows(RuntimeException.class, () -> {
            csvProductMachine = new CsvProductMachine(CsvProductMachineTest.class.getResource("/sample-csvs").getPath(), 10, 2, 4);
            csvProductMachine.getResults();
        });
    }

    @Test
    public void getResultsWhenProcessingWasFinishedTest() {
        csvProductMachine = new CsvProductMachine(CsvProductMachineTest.class.getResource("/sample-csvs").getPath(), 10, 2, 4);
        csvProductMachine.start();
        while (true) {
            if (csvProductMachine.isProcessingDone()) {
                assertEquals(csvProductMachine.preparedFiles().size(), 5);
                assertEquals(csvProductMachine.getResults().size(), 10);
                break;
            }
        }
    }
}
