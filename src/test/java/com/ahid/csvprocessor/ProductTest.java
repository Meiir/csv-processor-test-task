package com.ahid.csvprocessor;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    @Test
    public void productSortTest() {
        List<Product> products = Arrays.asList(
                new Product(1, "first", "", "", 3.6f),
                new Product(2, "second", "", "", 4.1f),
                new Product(3, "third", "", "", 0.6f),
                new Product(4, "fourth", "", "", 0.7f),
                new Product(5, "fifth", "", "", 8.6f),
                new Product(5, "fifth", "", "", 0.8f)
        );

        Collections.sort(products);
        assertEquals(products.get(0).getPrice(), 0.6f);
        assertEquals(products.get(1).getPrice(), 0.7f);
        assertEquals(products.get(2).getPrice(), 0.8f);
        assertEquals(products.get(5).getPrice(), 8.6f);
    }
}
