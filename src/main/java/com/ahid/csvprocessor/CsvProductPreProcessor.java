package com.ahid.csvprocessor;

import java.io.File;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import static com.ahid.csvprocessor.CsvUtil.csvToProducts;

public class CsvProductPreProcessor implements Runnable {

    private int resultRowCnt;
    private int sameIdCnt;
    private File file;
    private Queue<Product> preprocessedProducts;
    private AtomicInteger handledPreprocessorsCount;

    public CsvProductPreProcessor(int resultRowCnt, int sameIdCnt, File file, Queue<Product> preprocessedProducts, AtomicInteger handledPreprocessorsCount) {
        this.resultRowCnt = resultRowCnt;
        this.sameIdCnt = sameIdCnt;
        this.file = file;
        this.preprocessedProducts = preprocessedProducts;
        this.handledPreprocessorsCount = handledPreprocessorsCount;
    }

    @Override
    public void run() {
        System.out.println("CSV is processing...");
        List<Product> preprocessed = csvToProducts(this.file);
        preprocessed.parallelStream().sorted().limit(resultRowCnt).forEach(product -> preprocessedProducts.add(product));
        handledPreprocessorsCount.incrementAndGet();
    }
}
