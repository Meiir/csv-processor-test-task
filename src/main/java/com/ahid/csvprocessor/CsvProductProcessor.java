package com.ahid.csvprocessor;

import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class CsvProductProcessor implements Runnable {

    private int resultRowCnt;
    private int sameIdCnt;
    private Queue<Product> preprocessedProducts;
    private List<Product> results;
    private Supplier<Boolean> finishAction;

    public CsvProductProcessor(int resultRowCnt, int sameIdCnt, Queue<Product> preprocessedProducts, List<Product> results, Supplier<Boolean> finishAction) {
        this.resultRowCnt = resultRowCnt;
        this.sameIdCnt = sameIdCnt;
        this.preprocessedProducts = preprocessedProducts;
        this.results = results;
        this.finishAction = finishAction;
    }

    @Override
    public void run() {
        while (true) {
            Product product;
            if ((product = preprocessedProducts.poll()) != null) {
                System.out.println("CSV is processing...");

                // we add items to results without sorting until results size not equal to 'resultRowCnt'
                int prevsize = results.size();

                if (prevsize == resultRowCnt - 1) {
                    results.add(product);
                    Collections.sort(results);
                } else if (prevsize == resultRowCnt) {
                    Product last = results.get(results.size() - 1);
                    if (product.getPrice() < last.getPrice()) {
                        results.set(results.size() - 1, product);
                        Collections.sort(results);
                    }
                } else {
                    results.add(product);
                }

                // check for products with same id, there is limit for same Ids -> sameIdCnt
                List<Product> withSameId = results
                        .stream()
                        .filter(item -> product.getProductId() == item.getProductId())
                        .sorted()
                        .skip(sameIdCnt)
                        .collect(Collectors.toList());
                if (!withSameId.isEmpty()) {
                    withSameId.forEach(willBeRemoved -> results.remove(willBeRemoved));
                }

            } else {
                if (this.finishAction.get()) {
                    if (results.size() < resultRowCnt) {
                        Collections.sort(results);
                    }
                    break;
                }
            }
        }
    }
}
