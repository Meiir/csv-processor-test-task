package com.ahid.csvprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class CsvProductMachine {

    private CopyOnWriteArrayList<File> files = new CopyOnWriteArrayList();
    private int resultRowCnt;
    private int sameIdCnt;

    private ExecutorService executorService;

    private AtomicBoolean isProcessingDone;
    private Queue<Product> preprocessedProducts;
    private AtomicInteger handledPreprocessorsCount;
    private List<Product> results;

    public CsvProductMachine(String rootDir, int resultRowCnt, int sameIdCnt, int countOfPreProcessors) {
        this.resultRowCnt = resultRowCnt;
        this.sameIdCnt = sameIdCnt;
        this.prepareFiles(rootDir);
        executorService = Executors.newFixedThreadPool(countOfPreProcessors);
        results = new CopyOnWriteArrayList<>();
        preprocessedProducts = new ConcurrentLinkedQueue<>();
        handledPreprocessorsCount = new AtomicInteger(0);
        isProcessingDone = new AtomicBoolean(false);
    }

    public void start() {
        files.forEach(file -> {
            CsvProductPreProcessor csvProductPreProcessor = new CsvProductPreProcessor(resultRowCnt, sameIdCnt, file, preprocessedProducts, handledPreprocessorsCount);
            executorService.submit(csvProductPreProcessor);
        });

        new Thread(new CsvProductProcessor(resultRowCnt, sameIdCnt, preprocessedProducts, results, () -> {
            if (handledPreprocessorsCount.get() == files.size()) {
                executorService.shutdown();
                isProcessingDone.compareAndSet(false, true);
                return true;
            } else
                return false;
        })).start();
    }

    private void prepareFiles(String rootDir) {
        try (Stream<Path> walk = Files.walk(Paths.get(rootDir))) {
            walk.filter(path -> path.toString().endsWith(".csv"))
                    .map(path -> path.toFile())
                    .forEach(file -> files.add(file));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isProcessingDone() {
        return isProcessingDone.get();
    }

    public List<File> preparedFiles() {
        return Collections.unmodifiableList(files);
    }

    public List<Product> getResults() {
        if (!isProcessingDone.get())
            throw new RuntimeException("It is still processing");
        return results;
    }
}
