package com.ahid.csvprocessor;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        CommandOptions cmd = new CommandOptions(args);
        String csvDir = null;
        String resultFile = null;
        int workersCount = Runtime.getRuntime().availableProcessors() + 2;
        int resultRowCnt = 1000;
        int sameIdCnt = 20;
        if (cmd.hasOption("-i")) {
            csvDir = cmd.valueOf("-i");
        }
        if (cmd.hasOption("-o")) {
            resultFile = cmd.valueOf("-o");
        }

        if (csvDir == null)
            throw new RuntimeException("Missed required parameter - directory of CSV-files");
        if (resultFile == null)
            throw new RuntimeException("Missed required parameter - location of result CSV-File");

        CsvProductMachine csvProductMachine = new CsvProductMachine(csvDir, resultRowCnt, sameIdCnt, workersCount);
        csvProductMachine.start();
        while (true) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (csvProductMachine.isProcessingDone()) {
                List<Product> results = csvProductMachine.getResults();
                File file = new File(resultFile);
                CsvUtil.productsToCsv(results, file);
                System.out.println("Finished");
                break;
            }
        }
    }
}
