package com.ahid.csvprocessor;

import com.opencsv.bean.CsvBindByName;

import java.util.Objects;

public class Product implements Comparable<Product> {

    @CsvBindByName(column = "productID")
    private int productId;
    @CsvBindByName(column = "Name")
    private String name;
    @CsvBindByName(column = "Condition")
    private String condition;
    @CsvBindByName(column = "State")
    private String state;
    @CsvBindByName(column = "Price")
    private float price;

    public Product() {
    }

    public Product(int productId, String name, String condition, String state, float price) {
        this.productId = productId;
        this.name = name;
        this.condition = condition;
        this.state = state;
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId == product.productId &&
                Float.compare(product.price, price) == 0 &&
                Objects.equals(name, product.name) &&
                Objects.equals(condition, product.condition) &&
                Objects.equals(state, product.state);
    }

    @Override
    public int hashCode() {

        return Objects.hash(productId, name, condition, state, price);
    }

    @Override
    public int compareTo(Product o) {
        if (this.price < o.getPrice())
            return -1;
        if (this.price > o.getPrice())
            return 1;

        int thisBits = Float.floatToIntBits(this.price);
        int anotherBits = Float.floatToIntBits(o.getPrice());

        return (thisBits == anotherBits ? 0 : (thisBits < anotherBits ? -1 : 1));
    }
}
