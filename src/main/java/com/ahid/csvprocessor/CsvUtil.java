package com.ahid.csvprocessor;

import com.opencsv.CSVReader;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class CsvUtil {

    public static List<Product> csvToProducts(File file) {
        Map<String, String> mapping = new HashMap<>();
        mapping.put("productId", "productID");
        mapping.put("name", "Name");
        mapping.put("condition", "Condition");
        mapping.put("state", "State");
        mapping.put("price", "Price");

        HeaderColumnNameTranslateMappingStrategy<Product> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(Product.class);
        strategy.setColumnMapping(mapping);

        try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
            ;
            CsvToBean csvToBean = new CsvToBean();
            List<Product> products = csvToBean.parse(strategy, csvReader);
            return products;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void productsToCsv(List<Product> products, File file) {
        try (FileWriter writer = new FileWriter(file)) {
            ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
            mappingStrategy.setType(Product.class);

            String[] columns = new String[]{"productID", "Name", "Condition", "State", "Price"};
            mappingStrategy.setColumnMapping(columns);

            StatefulBeanToCsvBuilder<Product> builder = new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv beanWriter = builder.withMappingStrategy(mappingStrategy).build();

            beanWriter.write(products);
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            throw new RuntimeException(e);
        }
    }
}
